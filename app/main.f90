program flox
    use ast_printer_m, only: ast_printer_t
    use error_m, only: error, had_error
    use expr_m, only: expr_t
    use iso_varying_string, only: varying_string, get, put, put_line
    use parser_m, only: parser_t
    use scanner_m, only: scanner_t
    use strff, only: read_file
    use token_m, only: token_t

    implicit none

    if (command_argument_count() > 1) then
        call put_line("Usage: flox [script]")
        stop 64
    else if (command_argument_count() == 1) then
        call run_file(get_argument(1))
    else
        call run_prompt()
    end if
contains
    function get_argument(i) result(argument)
        integer, intent(in) :: i
        character(len=:), allocatable :: argument

        integer :: arg_len

        call get_command_argument(number=i, length=arg_len)
        allocate(character(len=arg_len) :: argument)
        call get_command_argument(number=i, value=argument)
    end function

    subroutine run_file(path)
        character(len=*), intent(in) :: path

        type(varying_string) :: contents

        contents = read_file(path)
        call run(contents)
        if (had_error) stop 65
    end subroutine

    subroutine run_prompt()
        type(varying_string) :: line
        integer :: iostat

        do
            call put("> ")
            call get(line, iostat=iostat)
            if (is_iostat_end(iostat)) exit
            call run(line)
            had_error = .false.
        end do
    end subroutine

    subroutine run(source)
        type(varying_string), intent(in) :: source

        class(expr_t), allocatable :: expr
        type(parser_t) :: parser
        type(ast_printer_t) :: printer
        type(scanner_t) :: scanner
        type(token_t), allocatable :: tokens(:)
        integer :: i

        scanner = scanner_t(source)
        tokens = scanner%scan_tokens()
        parser = parser_t(tokens)
        call parser%parse(expr)
        if (had_error) return
        call put_line(printer%print(expr))
    end subroutine
end program
