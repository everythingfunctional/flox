program ast_printer
    use ast_printer_m, only: ast_printer_t
    use expr_m
    use iso_varying_string, only: put_line, var_str
    use null_m, only: null_t
    use number_m, only: number_t
    use token_m, only: token_t
    use token_types_m

    implicit none

    type(ast_printer_t) :: printer

    call put_line(printer%print(binary_t( &
        unary_t(token_t(MINUS, var_str("-"), null_t(), 1), literal_t(number_t(123d0))), &
        token_t(STAR, var_str("*"), null_t(), 1), &
        grouping_t(literal_t(number_t(45.67d0))))))
end program