module ast_printer_m
    use exception_m, only: exception_t
    use expr_m
    use iso_varying_string, only: &
            varying_string, assignment(=), operator(//), var_str

    implicit none
    private
    public :: ast_printer_t

    type, extends(visitor_t) :: ast_printer_t
        type(varying_string) :: string
    contains
        procedure :: print
        procedure :: parenthesize
        procedure :: visit_binary
        procedure :: visit_grouping
        procedure :: visit_literal
        procedure :: visit_unary
    end type
contains
    function print(self, expr) result(string)
        class(ast_printer_t), intent(inout) :: self
        class(expr_t), intent(in) :: expr
        type(varying_string) :: string

        class(exception_t), allocatable :: exception

        self%string = ""
        call expr%accept(self, exception)
        string = self%string
    end function

    recursive subroutine parenthesize(self, exception, name, first, second)
        class(ast_printer_t), intent(inout) :: self
        class(exception_t), allocatable, intent(out) :: exception
        type(varying_string), intent(in) :: name
        class(expr_t), intent(in) :: first
        class(expr_t), optional, intent(in) :: second

        self%string = self%string // "(" // name // " "
        call first%accept(self, exception)
        if (present(second)) then
            self%string = self%string // " "
            call second%accept(self, exception)
        end if
        self%string = self%string // ")"
    end subroutine

    recursive subroutine visit_binary(self, expr, exception)
        class(ast_printer_t), intent(inout) :: self
        type(binary_t), intent(in) :: expr
        class(exception_t), allocatable, intent(out) :: exception

        call self%parenthesize(exception, expr%operator%lexeme, expr%left, expr%right)
    end subroutine

    recursive subroutine visit_grouping(self, expr, exception)
        class(ast_printer_t), intent(inout) :: self
        type(grouping_t), intent(in) :: expr
        class(exception_t), allocatable, intent(out) :: exception

        call self%parenthesize(exception, var_str("group"), expr%expression)
    end subroutine

    recursive subroutine visit_literal(self, expr, exception)
        class(ast_printer_t), intent(inout) :: self
        type(literal_t), intent(in) :: expr
        class(exception_t), allocatable, intent(out) :: exception

        self%string = self%string // expr%value%to_string()
    end subroutine

    recursive subroutine visit_unary(self, expr, exception)
        class(ast_printer_t), intent(inout) :: self
        type(unary_t), intent(in) :: expr
        class(exception_t), allocatable, intent(out) :: exception

        call self%parenthesize(exception, expr%operator%lexeme, expr%right)
    end subroutine
end module