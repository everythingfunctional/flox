module bool_m
    use iso_varying_string, only: varying_string
    use literal_value_m, only: literal_value_t
    use strff, only: to_string

    implicit none
    private
    public :: bool_t

    type, extends(literal_value_t) :: bool_t
        private
        logical :: val_
    contains
        procedure :: to_string => bool_to_string
    end type

    interface bool_t
        module procedure constructor
    end interface
contains
    pure function constructor(val) result(bool)
        logical, intent(in) :: val
        type(bool_t) :: bool

        bool%val_ = val
    end function

    pure function bool_to_string(self) result(string)
        class(bool_t), intent(in) :: self
        type(varying_string) :: string

        string = to_string(self%val_)
    end function
end module