module error_m
    use iso_varying_string, only: varying_string, operator(//), put_line, var_str
    use strff, only: to_string
    use token_m, only: token_t
    use token_types_m, only: EOF

    implicit none
    private
    public :: error, had_error

    interface error
        module procedure line_error
        module procedure token_error
    end interface

    logical :: had_error = .false.
contains
    subroutine line_error(line, message)
        integer, intent(in) :: line
        type(varying_string), intent(in) :: message

        call report(line, var_str(""), message)
    end subroutine

    subroutine token_error(token, message)
        type(token_t), intent(in) :: token
        type(varying_string), intent(in) :: message

        if (token%type == EOF) then
            call report(token%line, var_str(" at end"), message)
        else
            call report(token%line, " at '" // token%lexeme // "'", message)
        end if
    end subroutine

    subroutine report(line, where, message)
        integer, intent(in) :: line
        type(varying_string), intent(in) :: where
        type(varying_string), intent(in) :: message

        call put_line( &
                "[line " // to_string(line) // "] Error" // where // ": " // message)
        had_error = .true.
    end subroutine
end module