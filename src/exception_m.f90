module exception_m
    implicit none
    private
    public :: exception_t

    type, abstract :: exception_t
    contains
        procedure(string_i), deferred :: to_string
    end type

    abstract interface
        function string_i(self) result(string)
            use iso_varying_string, only: varying_string
            import exception_t
            implicit none
            class(exception_t), intent(in) :: self
            type(varying_string) :: string
        end function
    end interface
end module