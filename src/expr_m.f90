module expr_m
    use exception_m, only: exception_t
    use literal_value_m, only: literal_value_t
    use token_m, only: token_t

    implicit none
    private
    public :: &
            binary_t, &
            grouping_t, &
            literal_t, &
            unary_t, &
            expr_t, &
            visitor_t

    type, abstract :: visitor_t
    contains
        procedure(visit_binary_i), deferred :: visit_binary
        procedure(visit_grouping_i), deferred :: visit_grouping
        procedure(visit_literal_i), deferred :: visit_literal
        procedure(visit_unary_i), deferred :: visit_unary
    end type

    type, abstract :: expr_t
    contains
        procedure(accept_i), deferred :: accept
    end type

    type, extends(expr_t) :: binary_t
        class(expr_t), allocatable :: left
        class(token_t), allocatable :: operator
        class(expr_t), allocatable :: right
    contains
        procedure :: accept => binary_accept
    end type

    type, extends(expr_t) :: grouping_t
        class(expr_t), allocatable :: expression
    contains
        procedure :: accept => grouping_accept
    end type

    type, extends(expr_t) :: literal_t
        class(literal_value_t), allocatable :: value
    contains
        procedure :: accept => literal_accept
    end type

    type, extends(expr_t) :: unary_t
        class(token_t), allocatable :: operator
        class(expr_t), allocatable :: right
    contains
        procedure :: accept => unary_accept
    end type

    abstract interface
        recursive subroutine accept_i(self, visitor, exception)
            import :: exception_t, expr_t, visitor_t
            implicit none
            class(expr_t), intent(in) :: self
            class(visitor_t), intent(inout) :: visitor
            class(exception_t), allocatable, intent(out) :: exception
        end subroutine

        recursive subroutine visit_binary_i(self, expr, exception)
            import :: binary_t, exception_t, visitor_t
            implicit none
            class(visitor_t), intent(inout) :: self
            type(binary_t), intent(in) :: expr
            class(exception_t), allocatable, intent(out) :: exception
        end subroutine

        recursive subroutine visit_grouping_i(self, expr, exception)
            import :: exception_t, grouping_t, visitor_t
            implicit none
            class(visitor_t), intent(inout) :: self
            type(grouping_t), intent(in) :: expr
            class(exception_t), allocatable, intent(out) :: exception
        end subroutine

        recursive subroutine visit_literal_i(self, expr, exception)
            import :: exception_t, literal_t, visitor_t
            implicit none
            class(visitor_t), intent(inout) :: self
            type(literal_t), intent(in) :: expr
            class(exception_t), allocatable, intent(out) :: exception
        end subroutine

        recursive subroutine visit_unary_i(self, expr, exception)
            import :: exception_t, unary_t, visitor_t
            implicit none
            class(visitor_t), intent(inout) :: self
            type(unary_t), intent(in) :: expr
            class(exception_t), allocatable, intent(out) :: exception
        end subroutine
    end interface

    interface binary_t
        module procedure binary_constructor
    end interface

    interface grouping_t
        module procedure grouping_constructor
    end interface

    interface literal_t
        module procedure literal_constructor
    end interface

    interface unary_t
        module procedure unary_constructor
    end interface
contains
    recursive subroutine binary_accept(self, visitor, exception)
        class(binary_t), intent(in) :: self
        class(visitor_t), intent(inout) :: visitor
        class(exception_t), allocatable, intent(out) :: exception

        call visitor%visit_binary(self, exception)
    end subroutine

    recursive subroutine grouping_accept(self, visitor, exception)
        class(grouping_t), intent(in) :: self
        class(visitor_t), intent(inout) :: visitor
        class(exception_t), allocatable, intent(out) :: exception

        call visitor%visit_grouping(self, exception)
    end subroutine

    recursive subroutine literal_accept(self, visitor, exception)
        class(literal_t), intent(in) :: self
        class(visitor_t), intent(inout) :: visitor
        class(exception_t), allocatable, intent(out) :: exception

        call visitor%visit_literal(self, exception)
    end subroutine

    recursive subroutine unary_accept(self, visitor, exception)
        class(unary_t), intent(in) :: self
        class(visitor_t), intent(inout) :: visitor
        class(exception_t), allocatable, intent(out) :: exception

        call visitor%visit_unary(self, exception)
    end subroutine

    function binary_constructor(left, operator, right) result(binary)
        class(expr_t), intent(in) :: left
        class(token_t), intent(in) :: operator
        class(expr_t), intent(in) :: right
        type(binary_t) :: binary

        binary%left = left
        binary%operator = operator
        binary%right = right
    end function

    function grouping_constructor(expression) result(grouping)
        class(expr_t), intent(in) :: expression
        type(grouping_t) :: grouping

        grouping%expression = expression
    end function

    function literal_constructor(value) result(literal)
        class(literal_value_t), intent(in) :: value
        type(literal_t) :: literal

        literal%value = value
    end function

    function unary_constructor(operator, right) result(unary)
        class(token_t), intent(in) :: operator
        class(expr_t), intent(in) :: right
        type(unary_t) :: unary

        unary%operator = operator
        unary%right = right
    end function
end module

