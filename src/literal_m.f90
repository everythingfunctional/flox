module literal_value_m
    implicit none
    private
    public :: literal_value_t

    type, abstract :: literal_value_t
    contains
        procedure(to_string_i), deferred :: to_string
    end type

    abstract interface
        pure function to_string_i(self) result(string)
            use iso_varying_string, only: varying_string
            import :: literal_value_t
            implicit none
            class(literal_value_t), intent(in) :: self
            type(varying_string) :: string
        end function
    end interface
end module