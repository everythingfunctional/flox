module null_m
    use iso_varying_string, only: varying_string, assignment(=)
    use literal_value_m, only: literal_value_t

    implicit none
    private
    public :: null_t

    type, extends(literal_value_t) :: null_t
    contains
        procedure :: to_string
    end type
contains
    pure function to_string(self) result(string)
        class(null_t), intent(in) :: self
        type(varying_string) :: string

        associate(unused => self); end associate
        string = "null"
    end function
end module