module number_m
    use iso_varying_string, only: varying_string
    use literal_value_m, only: literal_value_t
    use strff, only: to_string

    implicit none
    private
    public :: number_t

    type, extends(literal_value_t) :: number_t
        private
        double precision :: val_
    contains
        procedure :: to_string => number_to_string
    end type

    interface number_t
        module procedure constructor
    end interface
contains
    pure function constructor(val) result(number)
        double precision, intent(in) :: val
        type(number_t) :: number

        number%val_ = val
    end function

    pure function number_to_string(self) result(string)
        class(number_t), intent(in) :: self
        type(varying_string) :: string

        string = to_string(self%val_)
    end function
end module