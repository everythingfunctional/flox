module parser_m
    use bool_m, only: bool_t
    use error_m, only: report_error => error
    use expr_m
    use iso_varying_string, only: varying_string, var_str
    use null_m, only: null_t
    use token_m, only: token_t
    use token_types_m

    implicit none
    private
    public :: parser_t

    type :: parser_t
        private
        type(token_t), allocatable :: tokens(:)
        integer :: current = 1
    contains
        procedure :: parse
        procedure :: expression
        procedure :: equality
        procedure :: comparison
        procedure :: term
        procedure :: factor
        procedure :: unary
        procedure :: primary
        procedure :: match
        procedure :: consume
        procedure :: check
        procedure :: advance
        procedure :: is_at_end
        procedure :: peek
        procedure :: previous
        procedure :: synchronize
    end type

    type :: parser_error_t
    end type

    interface parser_t
        module procedure constructor
    end interface
contains
    function constructor(tokens) result(parser)
        type(token_t), intent(in) :: tokens(:)
        type(parser_t) :: parser

        parser%tokens = tokens
    end function

    subroutine parse(self, expr)
        class(parser_t), intent(inout) :: self
        class(expr_t), allocatable, intent(out) :: expr

        class(parser_error_t), allocatable :: error

        call self%expression(expr, error)
        if (allocated(expr) .and. allocated(error)) deallocate(expr)
    end subroutine

    recursive subroutine expression(self, expr, error)
        class(parser_t), intent(inout) :: self
        class(expr_t), allocatable, intent(out) :: expr
        class(parser_error_t), allocatable, intent(out) :: error

         call self%equality(expr, error)
    end subroutine

    recursive subroutine equality(self, expr, error)
        class(parser_t), intent(inout) :: self
        class(expr_t), allocatable, intent(out) :: expr
        class(parser_error_t), allocatable, intent(out) :: error

        call self%comparison(expr, error)
        if (allocated(error)) return
        do while (self%match([BANG_EQUAL, EQUAL_EQUAL]))
        block
            type(token_t) :: operator
            class(expr_t), allocatable :: right, new

            operator = self%previous()
            call self%comparison(right, error)
            if (allocated(error)) return
            new = binary_t(expr, operator, right)
            deallocate(expr)
            call move_alloc(new, expr)
        end block
        end do
    end subroutine

    recursive subroutine comparison(self, expr, error)
        class(parser_t), intent(inout) :: self
        class(expr_t), allocatable, intent(out) :: expr
        class(parser_error_t), allocatable, intent(out) :: error

        call self%term(expr, error)
        if (allocated(error)) return
        do while (self%match([GREATER, GREATER_EQUAL, LESS, LESS_EQUAL]))
        block
            type(token_t) :: operator
            class(expr_t), allocatable :: right, new

            operator = self%previous()
            call self%term(right, error)
            if (allocated(error)) return
            new = binary_t(expr, operator, right)
            deallocate(expr)
            call move_alloc(new, expr)
        end block
        end do
    end subroutine

    recursive subroutine term(self, expr, error)
        class(parser_t), intent(inout) :: self
        class(expr_t), allocatable, intent(out) :: expr
        class(parser_error_t), allocatable, intent(out) :: error

        call self%factor(expr, error)
        if (allocated(error)) return
        do while (self%match([MINUS, PLUS]))
        block
            type(token_t) :: operator
            class(expr_t), allocatable :: right, new

            operator = self%previous()
            call self%factor(right, error)
            if (allocated(error)) return
            new = binary_t(expr, operator, right)
            deallocate(expr)
            call move_alloc(new, expr)
        end block
        end do
    end subroutine

    recursive subroutine factor(self, expr, error)
        class(parser_t), intent(inout) :: self
        class(expr_t), allocatable, intent(out) :: expr
        class(parser_error_t), allocatable, intent(out) :: error

        call self%unary(expr, error)
        if (allocated(error)) return
        do while (self%match([SLASH, STAR]))
        block
            type(token_t) :: operator
            class(expr_t), allocatable :: right, new

            operator = self%previous()
            call self%unary(right, error)
            if (allocated(error)) return
            new = binary_t(expr, operator, right)
            deallocate(expr)
            call move_alloc(new, expr)
        end block
        end do
    end subroutine

    recursive subroutine unary(self, expr, error)
        class(parser_t), intent(inout) :: self
        class(expr_t), allocatable, intent(out) :: expr
        class(parser_error_t), allocatable, intent(out) :: error

        if (self%match([BANG, MINUS])) then
        block
            type(token_t) :: operator
            class(expr_t), allocatable :: next

            operator = self%previous()
            call self%unary(next, error)
            if (allocated(error)) return
            expr = unary_t(operator, next)
        end block
        else
            call self%primary(expr, error)
        end if
    end subroutine

    recursive subroutine primary(self, expr, error)
        class(parser_t), intent(inout) :: self
        class(expr_t), allocatable, intent(out) :: expr
        class(parser_error_t), allocatable, intent(out) :: error

        if (self%match([FALSE])) then
            expr = literal_t(bool_t(.false.))
        else if (self%match([TRUE])) then
            expr = literal_t(bool_t(.true.))
        else if (self%match([NIL])) then
            expr = literal_t(null_t())
        else if (self%match([[NUMBER, STRING]])) then
        block
            type(token_t) :: literal

            literal = self%previous()
            expr = literal_t(literal%literal)
        end block
        else if (self%match([LEFT_PAREN])) then
        block
            class(expr_t), allocatable :: inner

            call self%expression(inner, error)
            if (allocated(error)) return
            call self%consume(RIGHT_PAREN, var_str("Expect ')' after expression."), error)
            if (allocated(error)) return
            expr = grouping_t(inner)
        end block
        else
            call throw_error(self%peek(), var_str("Expect expression."), error)
        end if
    end subroutine

    function match(self, types)
        class(parser_t), intent(inout) :: self
        integer, intent(in) :: types(:)
        logical :: match

        integer :: i

        do i = 1, size(types)
            if (self%check(types(i))) then
                call self%advance()
                match = .true.
                return
            end if
        end do
        match = .false.
    end function

    subroutine consume(self, type, message, error)
        class(parser_t), intent(inout) :: self
        integer, intent(in) :: type
        type(varying_string), intent(in) :: message
        class(parser_error_t), allocatable, intent(out) :: error

        if (self%check(type)) then
            call self%advance()
        else
            call throw_error(self%peek(), message, error)
        end if
    end subroutine

    function check(self, type)
        class(parser_t), intent(in) :: self
        integer, intent(in) :: type
        logical :: check

        if (self%is_at_end()) then
            check = .false.
        else
        block
            type(token_t) :: next

            next = self%peek()
            check = next%type == type
        end block
        end if
    end function

    subroutine advance(self)
        class(parser_t), intent(inout) :: self

        if (.not. self%is_at_end()) self%current = self%current + 1
    end subroutine

    function is_at_end(self)
        class(parser_t), intent(in) :: self
        logical :: is_at_end

        type(token_t) :: next

        next = self%peek()
        is_at_end = next%type == EOF
    end function

    function peek(self)
        class(parser_t), intent(in) :: self
        type(token_t) :: peek

        peek = self%tokens(self%current)
    end function

    function previous(self)
        class(parser_t), intent(in) :: self
        type(token_t) :: previous

        previous = self%tokens(self%current - 1)
    end function

    subroutine throw_error(token, message, error)
        type(token_t), intent(in) :: token
        type(varying_string), intent(in) :: message
        class(parser_error_t), allocatable, intent(out) :: error

        call report_error(token, message)
        error = parser_error_t()
    end subroutine

    subroutine synchronize(self)
        class(parser_t), intent(inout) :: self

        call self%advance()
        do while (.not. self%is_at_end())
            associate(prev => self%previous())
                if (prev%type == SEMICOLON) return
            end associate
            associate(curr => self%peek())
                select case (curr%type)
                case (CLASS, FOR, FUN, IF, PRINT, RETURN, VAR, WHILE)
                    return
                end select
            end associate
            call self%advance()
        end do
    end subroutine
end module