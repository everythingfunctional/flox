module runtime_exception_m
    use exception_m, only: exception_t
    use iso_varying_string, only: varying_string, assignment(=)

    implicit none
    private
    public :: runtime_exception_t

    type, extends(exception_t) :: runtime_exception_t
    contains
        procedure :: to_string
    end type
contains
    function to_string(self) result(string)
        class(runtime_exception_t), intent(in) :: self
        type(varying_string) :: string

        string = "An Exception!"
    end function
end module