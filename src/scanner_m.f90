module scanner_m
    use error_m, only: error
    use iso_varying_string, only: varying_string, char, var_str
    use literal_value_m, only: literal_value_t
    use null_m, only: null_t
    use number_m, only: number_t
    use string_m, only: string_t
    use strff, only: NEWLINE
    use token_m, only: token_t
    use token_list_m, only: token_list_t
    use token_types_m

    implicit none
    private
    public :: scanner_t

    type :: scanner_t
        private
        character(len=:), allocatable :: source
        integer :: source_length
        integer :: start, current, line
    contains
        private
        procedure, public :: scan_tokens
        procedure :: scan_token
        procedure :: scan_string
        procedure :: scan_number
        procedure :: scan_identifier
        procedure :: is_at_end
        procedure :: advance
        procedure :: match
        procedure :: peek
        procedure :: peek_next
        procedure :: add_token_just_type
        procedure :: add_token_with_literal
        generic :: add_token => add_token_just_type, add_token_with_literal
    end type

    interface scanner_t
        module procedure constructor
    end interface
contains
    pure function constructor(source) result(scanner)
        type(varying_string), intent(in) :: source
        type(scanner_t) :: scanner

        scanner%source = char(source)
        scanner%source_length = len(scanner%source)
        scanner%start = 1
        scanner%current = 1
        scanner%line = 1
    end function

    function scan_tokens(self) result(tokens)
        class(scanner_t), intent(inout) :: self
        type(token_t), allocatable :: tokens(:)

        type(token_list_t) :: tokens_

        do while (.not. self%is_at_end())
            self%start = self%current
            call self%scan_token(tokens_)
        end do
        call tokens_%add(token_t(EOF, var_str(""), null_t(), self%line))
        call tokens_%to_array(tokens)
    end function

    subroutine scan_token(self, tokens)
        class(scanner_t), intent(inout) :: self
        type(token_list_t), intent(inout) :: tokens

        character(len=1), parameter :: TAB = char(9)
        character(len=1), parameter :: CARRIAGE_RETURN = char(13)
        character(len=1), parameter :: SPACE = char(32)
        character(len=1) :: c

        c = self%advance()
        select case (c)
        case ('(')
            call self%add_token(LEFT_PAREN, tokens)
        case (')')
            call self%add_token(RIGHT_PAREN, tokens)
        case ('{')
            call self%add_token(LEFT_BRACE, tokens)
        case ('}')
            call self%add_token(RIGHT_BRACE, tokens)
        case (',')
            call self%add_token(COMMA, tokens)
        case ('.')
            call self%add_token(DOT, tokens)
        case ('-')
            call self%add_token(MINUS, tokens)
        case ('+')
            call self%add_token(PLUS, tokens)
        case (';')
            call self%add_token(SEMICOLON, tokens)
        case ('*')
            call self%add_token(STAR, tokens)
        case ('!')
            call self%add_token(merge(BANG_EQUAL, BANG, self%match('=')), tokens)
        case ('=')
            call self%add_token(merge(EQUAL_EQUAL, EQUAL, self%match('=')), tokens)
        case ('<')
            call self%add_token(merge(LESS_EQUAL, LESS, self%match('=')), tokens)
        case ('>')
            call self%add_token(merge(GREATER_EQUAL, GREATER, self%match('=')), tokens)
        case ('/')
            if (self%match('/')) then
                do while (self%peek() /= NEWLINE .and. .not.self%is_at_end())
                    associate(unused => self%advance()); end associate
                end do
            else
                call self%add_token(SLASH, tokens)
            end if
        case (TAB, CARRIAGE_RETURN, SPACE)
        case (NEWLINE)
            self%line = self%line + 1
        case ('"')
            call self%scan_string(tokens)
        case default
            if (is_digit(c)) then
                call self%scan_number(tokens)
            else if (is_alpha(c)) then
                call self%scan_identifier(tokens)
            else
                call error(self%line, var_str("Unexpected character."))
            end if
        end select
    end subroutine

    subroutine scan_string(self, tokens)
        class(scanner_t), intent(inout) :: self
        type(token_list_t), intent(inout) :: tokens

        type(string_t) :: val

        do while (self%peek() /= '"' .and. .not.self%is_at_end())
            if (self%peek() == NEWLINE) self%line = self%line + 1
            associate(unused => self%advance()); end associate
        end do
        if (self%is_at_end()) then
            call error(self%line, var_str("Unterminated string."))
            return
        end if
        associate(unused => self%advance()); end associate ! the closing ".

        ! Trim the surrounding quotes.
        val = string_t(self%source(self%start+1 : self%current-2))
        call self%add_token(STRING, val, tokens)
    end subroutine

    subroutine scan_number(self, tokens)
        class(scanner_t), intent(inout) :: self
        type(token_list_t), intent(inout) :: tokens

        type(number_t) :: val
        double precision :: num

        do while (is_digit(self%peek()))
            associate(unused => self%advance()); end associate
        end do
        ! Look for a fractional part.
        if (self%peek() == '.' .and. is_digit(self%peek_next())) then
            ! Consume the "."
            associate(unused => self%advance()); end associate
            do while (is_digit(self%peek()))
                associate(unused => self%advance()); end associate
            end do
        end if
        read(self%source(self%start : self%current-1), *) num
        val = number_t(num)
        call self%add_token(NUMBER, val, tokens)
    end subroutine

    subroutine scan_identifier(self, tokens)
        class(scanner_t), intent(inout) :: self
        type(token_list_t), intent(inout) :: tokens

        integer :: type
        do while (is_alphanumeric(self%peek()))
            associate(unused => self%advance()); end associate
        end do
        type = get_keyword_type(self%source(self%start:self%current-1))
        if (type == UNKNOWN) type = IDENTIFIER
        call self%add_token(type, tokens)
    end subroutine

    pure function is_at_end(self)
        class(scanner_t), intent(in) :: self
        logical :: is_at_end

        is_at_end = self%current > self%source_length
    end function

    function advance(self) result(c)
        class(scanner_t), intent(inout) :: self
        character(len=1) :: c

        c = self%source(self%current:self%current)
        self%current = self%current + 1
    end function

    function match(self, expected)
        class(scanner_t), intent(inout) :: self
        character(len=1), intent(in) :: expected
        logical :: match

        if (self%is_at_end()) then
            match = .false.
            return
        end if
        if (self%source(self%current:self%current) /= expected) then
            match = .false.
            return
        end if
        self%current = self%current + 1
        match = .true.
    end function

    function peek(self)
        class(scanner_t), intent(in) :: self
        character(len=1) :: peek

        if (self%is_at_end()) then
            peek = char(0)
        else
            peek = self%source(self%current:self%current)
        end if
    end function

    function peek_next(self)
        class(scanner_t), intent(in) :: self
        character(len=1) :: peek_next

        if (self%current + 1 > self%source_length) then
            peek_next = char(0)
        else
            peek_next = self%source(self%current+1:self%current+1)
        end if
    end function

    pure function is_digit(c)
        character(len=1), intent(in) :: c
        logical :: is_digit

        is_digit = c >= '0' .and. c <= '9'
    end function

    pure function is_alpha(c)
        character(len=1), intent(in) :: c
        logical :: is_alpha

        is_alpha = &
                (c >= 'a' .and. c <= 'z') &
                .or. (c >= 'A' .and. c <= 'Z') &
                .or. c == '_'
    end function

    pure function is_alphanumeric(c)
        character(len=1), intent(in) :: c
        logical :: is_alphanumeric

        is_alphanumeric = is_alpha(c) .or. is_digit(c)
    end function

    subroutine add_token_just_type(self, type, tokens)
        class(scanner_t), intent(in) :: self
        integer, intent(in) :: type
        type(token_list_t), intent(inout) :: tokens

        call self%add_token(type, null_t(), tokens)
    end subroutine

    subroutine add_token_with_literal(self, type, literal, tokens)
        class(scanner_t), intent(in) :: self
        integer, intent(in) :: type
        class(literal_value_t), intent(in) :: literal
        type(token_list_t), intent(inout) :: tokens

        call tokens%add(token_t( &
                type, &
                var_str(self%source(self%start:self%current-1)), &
                literal, &
                self%line))
    end subroutine
end module