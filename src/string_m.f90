module string_m
    use iso_varying_string, only: varying_string, assignment(=)
    use literal_value_m, only: literal_value_t

    implicit none
    private
    public :: string_t

    type, extends(literal_value_t) :: string_t
        private
        character(len=:), allocatable :: val_
    contains
        procedure :: to_string
    end type

    interface string_t
        module procedure constructor
    end interface
contains
    pure function constructor(val) result(string)
        character(len=*), intent(in) :: val
        type(string_t) :: string

        string%val_ = val
    end function

    pure function to_string(self) result(string)
        class(string_t), intent(in) :: self
        type(varying_string) :: string

        string = self%val_
    end function
end module