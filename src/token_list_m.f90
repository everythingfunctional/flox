module token_list_m
    use token_m, only: token_t

    implicit none
    private
    public :: token_list_t

    type :: token_list_t
        private
        type(token_t), allocatable :: tokens(:)
        integer :: num_tokens = 0, size = 0
    contains
        procedure :: add
        procedure :: to_array
    end type
contains
    subroutine add(self, token)
        class(token_list_t), intent(inout) :: self
        type(token_t), intent(in) :: token

        if (self%size == 0) then
            self%size = 10
            allocate(self%tokens(self%size))
        else if (self%num_tokens == self%size) then
            block
                type(token_t), allocatable :: tmp(:)

                call move_alloc(self%tokens, tmp)
                self%size = self%size*2
                allocate(self%tokens(self%size))
                self%tokens(1:self%num_tokens) = tmp
            end block
        end if
        self%num_tokens = self%num_tokens + 1
        self%tokens(self%num_tokens) = token
    end subroutine

    subroutine to_array(self, tokens)
        class(token_list_t), intent(in) :: self
        type(token_t), allocatable, intent(out) :: tokens(:)

        allocate(tokens, source = self%tokens(1:self%num_tokens))
    end subroutine
end module