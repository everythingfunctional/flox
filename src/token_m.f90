module token_m
    use iso_varying_string, only: varying_string, assignment(=), operator(//)
    use literal_value_m, only: literal_value_t
    use token_types_m

    implicit none
    private
    public :: token_t

    type :: token_t
        integer :: type
        type(varying_string) :: lexeme
        class(literal_value_t), allocatable :: literal
        integer :: line
    contains
        procedure :: to_string => token_to_string
    end type

    interface token_t
        module procedure constructor
    end interface
contains
    function constructor(type, lexeme, literal, line) result(token)
        integer, intent(in) :: type
        type(varying_string), intent(in) :: lexeme
        class(literal_value_t), intent(in) :: literal
        integer, intent(in) :: line
        type(token_t) :: token

        token%type = type
        token%lexeme = lexeme
        token%literal = literal
        token%line = line
    end function

    pure function token_to_string(self) result(string)
        class(token_t), intent(in) :: self
        type(varying_string) :: string

        string = &
                type_to_string(self%type) &
                // " " // self%lexeme &
                // " " // self%literal%to_string()
    end function

    pure function type_to_string(type)
        integer, intent(in) :: type
        type(varying_string) :: type_to_string

        select case (type)
        case (LEFT_PAREN)
            type_to_string = "("
        case (RIGHT_PAREN)
            type_to_string = ")"
        case (LEFT_BRACE)
            type_to_string = "{"
        case (RIGHT_BRACE)
            type_to_string = "}"
        case (COMMA)
            type_to_string = ","
        case (DOT)
            type_to_string = "."
        case (MINUS)
            type_to_string = "-"
        case (PLUS)
            type_to_string = "+"
        case (SEMICOLON)
            type_to_string = ";"
        case (SLASH)
            type_to_string = "/"
        case (STAR)
            type_to_string = "*"
        case (BANG)
            type_to_string = "!"
        case (BANG_EQUAL)
            type_to_string = "!="
        case (EQUAL)
            type_to_string = "="
        case (EQUAL_EQUAL)
            type_to_string = "=="
        case (GREATER)
            type_to_string = ">"
        case (GREATER_EQUAL)
            type_to_string = ">="
        case (LESS)
            type_to_string = "<"
        case (LESS_EQUAL)
            type_to_string = "<="
        case (IDENTIFIER)
            type_to_string = "identifier"
        case (STRING)
            type_to_string = "string"
        case (NUMBER)
            type_to_string = "number"
        case (AND)
            type_to_string = "AND"
        case (CLASS)
            type_to_string = "CLASS"
        case (ELSE)
            type_to_string = "ELSE"
        case (FALSE)
            type_to_string = "FALSE"
        case (FUN)
            type_to_string = "FUN"
        case (FOR)
            type_to_string = "FOR"
        case (IF)
            type_to_string = "IF"
        case (NIL)
            type_to_string = "NIL"
        case (OR)
            type_to_string = "OR"
        case (PRINT)
            type_to_string = "PRINT"
        case (RETURN)
            type_to_string = "RETURN"
        case (SUPER)
            type_to_string = "SUPER"
        case (THIS)
            type_to_string = "THIS"
        case (TRUE)
            type_to_string = "TRUE"
        case (VAR)
            type_to_string = "VAR"
        case (WHILE)
            type_to_string = "WHILE"
        case (EOF)
            type_to_string = "EOF"
        case default
            type_to_string = "UNKNOWN TYPE"
        end select
    end function
end module