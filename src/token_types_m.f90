module token_types_m
    implicit none

    enum, bind(C) ! token types
        enumerator :: &
                ! single-character tokens
                LEFT_PAREN, RIGHT_PAREN, LEFT_BRACE, RIGHT_BRACE, &
                COMMA, DOT, MINUS, PLUS, SEMICOLON, SLASH, STAR, &

                ! one or two character tokens
                BANG, BANG_EQUAL, EQUAL, EQUAL_EQUAL, &
                GREATER, GREATER_EQUAL, LESS, LESS_EQUAL, &

                ! literals
                IDENTIFIER, STRING, NUMBER, &

                ! keywords
                AND, CLASS, ELSE, FALSE, FUN, FOR, IF, NIL, OR, &
                PRINT, RETURN, SUPER, THIS, TRUE, VAR, WHILE, &

                EOF, UNKNOWN
    end enum
contains
    pure function get_keyword_type(text) result(type)
        character(len=*), intent(in) :: text
        integer :: type

        select case (text)
        case ("and")
            type = AND
        case ("class")
            type = CLASS
        case ("else")
            type = ELSE
        case ("false")
            type = FALSE
        case ("for")
            type = FOR
        case ("fun")
            type = FUN
        case ("if")
            type = IF
        case ("nil")
            type = NIL
        case ("or")
            type = OR
        case ("print")
            type = PRINT
        case ("return")
            type = RETURN
        case ("super")
            type = SUPER
        case ("this")
            type = THIS
        case ("true")
            type = TRUE
        case ("var")
            type = VAR
        case ("while")
            type = WHILE
        case default
            type = UNKNOWN
        end select
    end function
end module