program main
    use iso_varying_string, only: &
            varying_string, operator(//), adjustl, put_line, trim, var_str
    use strff, only: join, split_at

    implicit none
    character(len=:), allocatable :: output_dir

    if (command_argument_count() /= 1) then
        call put_line("Usage: generate_ast <output directory>")
        stop 64
    else
        output_dir = get_argument(1)
        call define_ast( &
                output_dir, &
                "expr", &
                [ var_str("binary   : expr left, token operator, expr right") &
                , var_str("grouping : expr expression") &
                , var_str("literal  : literal_value value") &
                , var_str("unary    : token operator, expr right") &
                ])
    end if
contains
    function get_argument(i) result(argument)
        integer, intent(in) :: i
        character(len=:), allocatable :: argument

        integer :: arg_len

        call get_command_argument(number=i, length=arg_len)
        allocate(character(len=arg_len) :: argument)
        call get_command_argument(number=i, value=argument)
    end function

    subroutine define_ast(output_dir, base_name, types)
        character(len=*), intent(in) :: output_dir
        character(len=*), intent(in) :: base_name
        type(varying_string), intent(in) :: types(:)

        character(len=:), allocatable :: path
        integer :: unit
        integer :: i

        path = output_dir // "/" // base_name // "_m.f90"
        open(file=path, newunit=unit, status="replace")

        call put_line(unit, "module " // base_name // "_m")
        call put_line(unit, "    use literal_value_m, only: literal_value_t")
        call put_line(unit, "    use token_m, only: token_t")
        call put_line(unit, "")
        call put_line(unit, "    implicit none")
        call put_line(unit, "    private")
        call put_line(unit, "    public :: &")
        do i = 1, size(types)
        block
            type(varying_string), allocatable :: parts(:)
            type(varying_string) :: type_name

            parts = split_at(types(i), ":")
            type_name = trim(parts(1))
            call put_line(unit, "            " // type_name // "_t, &")
        end block
        end do
        call put_line(unit, "            " // base_name // "_t, &")
        call put_line(unit, "            visitor_t")

        call put_line(unit, "")
        call put_line(unit, "    type, abstract :: visitor_t")
        call put_line(unit, "    contains")
        do i = 1, size(types)
        block
            type(varying_string), allocatable :: parts(:)
            type(varying_string) :: type_name

            parts = split_at(types(i), ":")
            type_name = trim(parts(1))
            call put_line(unit, "        procedure(visit_" // type_name // "_i), deferred :: visit_" // type_name)
        end block
        end do
        call put_line(unit, "    end type")
        call put_line(unit, "")
        call put_line(unit, "    type, abstract :: " // base_name // "_t")
        call put_line(unit, "    contains")
        call put_line(unit, "        procedure(accept_i), deferred :: accept")
        call put_line(unit, "    end type")

        do i = 1, size(types)
        block
            type(varying_string), allocatable :: parts(:)
            type(varying_string) :: type_name, fields

            parts = split_at(types(i), ":")
            type_name = trim(parts(1))
            fields = trim(parts(2))
            call define_type(unit, base_name, type_name, fields)
        end block
        end do

        call put_line(unit, "")
        call put_line(unit, "    abstract interface")
        call put_line(unit, "        recursive subroutine accept_i(self, visitor)")
        call put_line(unit, "            import :: " // base_name // "_t, visitor_t")
        call put_line(unit, "            implicit none")
        call put_line(unit, "            class(" // base_name // "_t), intent(in) :: self")
        call put_line(unit, "            class(visitor_t), intent(inout) :: visitor")
        call put_line(unit, "        end subroutine")
        do i = 1, size(types)
        block
            type(varying_string), allocatable :: parts(:)
            type(varying_string) :: type_name

            parts = split_at(types(i), ":")
            type_name = trim(parts(1))
            call define_visit_interface(unit, type_name)
        end block
        end do
        call put_line(unit, "    end interface")

        do i = 1, size(types)
        block
            type(varying_string), allocatable :: parts(:)
            type(varying_string) :: type_name

            parts = split_at(types(i), ":")
            type_name = trim(parts(1))
            call put_line(unit, "")
            call put_line(unit, "    interface " // type_name // "_t")
            call put_line(unit, "        module procedure " // type_name // "_constructor")
            call put_line(unit, "    end interface")
        end block
        end do

        call put_line(unit, "contains")
        do i = 1, size(types)
        block
            type(varying_string), allocatable :: parts(:)
            type(varying_string) :: type_name

            parts = split_at(types(i), ":")
            type_name = trim(parts(1))
            call define_accept(unit, type_name)
        end block
        end do

        do i = 1, size(types)
        block
            type(varying_string), allocatable :: parts(:)
            type(varying_string) :: type_name, fields

            parts = split_at(types(i), ":")
            type_name = trim(parts(1))
            fields = trim(parts(2))
            call define_constructor(unit, type_name, fields)
        end block
        end do

        call put_line(unit, "end module")
        close(unit)
    end subroutine

    subroutine define_type(unit, base_name, type_name, fields)
        integer, intent(in) :: unit
        character(len=*), intent(in) :: base_name
        type(varying_string), intent(in) :: type_name, fields

        integer :: i
        type(varying_string), allocatable :: field_list(:)

        field_list = split_at(fields, ",")

        call put_line(unit, "")
        call put_line(unit, "    type, extends(" // base_name // "_t) :: " // type_name // "_t")
        do i = 1, size(field_list)
        block
            type(varying_string), allocatable :: field_parts(:)
            type(varying_string) :: field_type, field_name

            field_parts = split_at(trim(adjustl(field_list(i))), " ")
            field_type = field_parts(1)
            field_name = field_parts(2)
            call put_line(unit, "        class(" // field_type // "_t), allocatable :: " // field_name)
        end block
        end do
        call put_line(unit, "    contains")
        call put_line(unit, "        procedure :: accept => " // type_name // "_accept")
        call put_line(unit, "    end type")
    end subroutine

    subroutine define_visit_interface(unit, type_name)
        integer, intent(in) :: unit
        type(varying_string), intent(in) :: type_name

        call put_line(unit, "")
        call put_line(unit, "        recursive subroutine visit_" // type_name // "_i(self, expr)")
        call put_line(unit, "            import :: " // type_name // "_t, visitor_t")
        call put_line(unit, "            implicit none")
        call put_line(unit, "            class(visitor_t), intent(inout) :: self")
        call put_line(unit, "            type(" // type_name // "_t), intent(in) :: expr")
        call put_line(unit, "        end subroutine")
    end subroutine

    subroutine define_accept(unit, type_name)
        integer, intent(in) :: unit
        type(varying_string), intent(in) :: type_name

        call put_line(unit, "")
        call put_line(unit, "    recursive subroutine " // type_name // "_accept(self, visitor)")
        call put_line(unit, "        class(" // type_name // "_t), intent(in) :: self")
        call put_line(unit, "        class(visitor_t), intent(inout) :: visitor")
        call put_line(unit, "")
        call put_line(unit, "        call visitor%visit_" // type_name // "(self)")
        call put_line(unit, "    end subroutine")
    end subroutine

    subroutine define_constructor(unit, type_name, fields)
        integer, intent(in) :: unit
        type(varying_string), intent(in) :: type_name, fields

        integer :: i, num_fields
        type(varying_string), allocatable :: field_list(:), field_types(:), field_names(:)

        field_list = split_at(fields, ",")
        num_fields = size(field_list)
        allocate(field_types(num_fields), field_names(num_fields))
        do i = 1, num_fields
        block
            type(varying_string), allocatable :: field_parts(:)

            field_parts = split_at(trim(adjustl(field_list(i))), " ")
            field_types(i) = field_parts(1)
            field_names(i) = field_parts(2)
        end block
        end do

        call put_line(unit, "")
        call put_line(&
            unit, &
            "    function " // type_name // "_constructor(" &
            // join(field_names, ", ") // ") result(" // type_name // ")")

        do i = 1, num_fields
        block
            call put_line(unit, "        class(" // field_types(i) // "_t), intent(in) :: " // field_names(i))
        end block
        end do
        call put_line(unit, "        type(" // type_name // "_t) :: " // type_name)
        call put_line(unit, "")
        do i = 1, num_fields
        block
            call put_line(unit, "        " // type_name // "%" // field_names(i) // " = " // field_names(i))
        end block
        end do
        call put_line(unit, "    end function")
    end subroutine
end program
